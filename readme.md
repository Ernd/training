[![pipeline status](https://gitlab.com/crafting-software/training/badges/master/pipeline.svg)](https://gitlab.com/crafting-software/training/commits/master)

# Crafting Software Training

## Crafting unit tests

- [Java](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_unit_tests/slides-java.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_unit_tests/slides-java-with-notes.pdf?job=training)]


## Crafting with TDD

- [Agnostic](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_tdd/slides-agnostic.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_tdd/slides-agnostic-with-notes.pdf?job=training)]


## Crafting with SOLID principles

- [Java](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_with_solid_principles/slides-java.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_with_solid_principles/slides-java-with-notes.pdf?job=training)]


## Crafting with object calithenic principles

- [Java](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_with_calisthenics/slides-java.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_with_calisthenics/slides-java-with-notes.pdf?job=training)]



# License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
