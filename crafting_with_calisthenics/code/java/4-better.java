class Permissions {
  private List<Right> permissions;

  public boolean canPerformAction(User user) { }
}

class Application {
  private Permissions permissions;

  private void performAction(User user) {
    if (permissions.canPerformAction(user)) {
      // do action
    }
  }
}
