class Car {
    private Speed speed;
    private int occupants;

    public void onBoardOccupant() { ... }

    public void dropOffOccupant() {
        if (occupants == 0) {
            throw new CarEmptyException();
        }
        occupants--;
    }

    public void accelerate(Speed delta) {
        speed.acceletate(delta);
    }

    public void decelerate(Speed delta) { ... }
}
