public static Board evolve(final Board input) {
    final StringBuilder builder = new StringBuilder();
    for (int y = 0; y < input.height; y++) {
        for (int x = 0; x < input.width; x++) {
            if (computeStatus(input.cell(x, y), input.livingNeighborsCount(x, y)) == LIVE) {
                builder.append('#');
            } else {
                builder.append('.');
            }
        }
    }
    return Board.board(input.width, input.height, builder.toString());
}
