class Name {
  Surname family;
  GivenNames given;
}

class Surname {
  String family;
}

class GivenNames {
  List<String> names;
}
