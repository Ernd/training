public static Board evolve(final Board input) {
    return Board.board(input.width, input.height, walkLines(input, new StringBuilder()));
}

private static String walkLines(Board input, StringBuilder builder) {
    for (int y = 0; y < input.height; y++) {
        walkCells(input, builder, y);
    }
    return builder.toString();
}

private static void walkCells(Board input, StringBuilder builder, int y) {
    for (int x = 0; x < input.width; x++) {
        evolveCell(input, builder, y, x);
    }
}

private static void evolveCell(Board input, StringBuilder builder, int y, int x) {
    if (computeStatus(input.cell(x, y), input.livingNeighborsCount(x, y)) == LIVE) {
        builder.append('#');
    } else {
        builder.append('.');
    }
}
