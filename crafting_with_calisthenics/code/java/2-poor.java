
public Access removeProjectPermission()
{
    Access access;
    if (permissions.isAdministrator(user)) {
      access = GRANTED;
    } else {
      access = DENIED;
    }
    return access;
}
